# Submodule

## allprojects

- git clone https://gitlab.com/software1480817/allprojects.git .
- hat Branches **master** und **develop**
- 
## Add mainproject

- git submodule add https://gitlab.com/software1480817/mainproject.git
- hat Branches **master** und **develop**

## Add Tools/Backuptool

- git submodule add https://gitlab.com/software1480817/tools/backuptool.git
- hat Branches **master** und **develop**

## init
- git submodule update --init


## remove submodule backuptool
- git rm backuptool